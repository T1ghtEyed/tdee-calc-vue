import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    stats: {
      gender: null,
      age: null,
      weight: null,
      height: null,
      activity: null,
      bodyFat: null,
    },
  },
  mutations: {
    changeStats(state, payload) {
      state.stats = { ...state.stats, ...payload };
    },
  },
  actions: {

  },
});
