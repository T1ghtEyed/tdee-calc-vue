export default class Calculator {
  constructor(stats) {
    this.gender = parseInt(stats.gender, 10);
    this.age = parseInt(stats.age, 10);
    this.mass = parseInt(stats.weight, 10);
    this.height = parseInt(stats.height, 10);
    this.activity = parseInt(stats.activity, 10);
    this.bodyFat = parseInt(stats.bodyFat, 10);
  }

  bmr() {
    return Math.round(
      (10 * this.mass + 6.25 * this.height - 5 * this.age)
      + (this.gender === 1 ? 5 : -151)
    );
  }

  activityBmr() {
    const coeffs = [1.2, 1.37, 1.55, 1.72, 1.9];
    const result = [];

    coeffs.forEach(coeff => result.push(Math.round(this.bmr() * coeff)));

    return result;
  }

  idealWeight() {
    const hamwi = Math.round((this.gender === 1 ? 48 : 45.5)
      + (this.gender === 1 ? 2.7 : 2.2)
      * (0.394 * this.height - 60));

    const devine = Math.round((this.gender === 1 ? 50 : 45.5) + 2.3 * (0.394 * this.height - 60));

    const robinson = Math.round((this.gender === 1 ? 52 : 49)
      + (this.gender === 1 ? 1.9 : 1.7)
      * (0.394 * this.height - 60));

    const miller = Math.round((this.gender === 1 ? 56.2 : 53.1)
      + (this.gender === 1 ? 1.41 : 1.36)
      * (0.394 * this.height - 60));


    return {
      'G.J. Hamwi Formula (1964)': hamwi,
      'B.J. Devine Formula (1974)': devine,
      'J.D. Robinson Formula (1983)': robinson,
      'D.R. Miller Formula (1983)': miller,
    };
  }

  bmi() {
    const bmi = parseFloat(this.mass / ((this.height / 100) ** 2)).toFixed(1);

    let label = null;

    if (bmi < 15) label = 'Very severely underweight';
    else if (bmi < 16) label = 'Severaly underweight';
    else if (bmi < 18.5) label = 'Underweight';
    else if (bmi < 25) label = 'Normal weight';
    else if (bmi < 30) label = 'Overweight';
    else if (bmi < 35) label = 'Moderately obese';
    else if (bmi < 40) label = 'Severely obese';
    else if (bmi < 45) label = 'Very severely obese';
    else if (bmi < 50) label = 'Morbidly obese';
    else if (bmi < 60) label = 'Super obese';
    else if (bmi > 60) label = 'Hyper obese';
    else label = 'default';

    return { label, value: bmi };
  }
}
